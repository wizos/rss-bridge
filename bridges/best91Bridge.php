<?php
class DouBanBridge extends BridgeAbstract {
	const MAINTAINER = 'Wizos';
	const NAME = '91-';
	const DESCRIPTION = '';
	const URI = 'http://pic.workgreat14.live';
	const API_URI = URI . '/index.php';
	const CACHE_TIMEOUT = 3000; // 5min

	public function collectData(){
		$html = getSimpleHTMLDOM(self::API_URI) or returnServerError(' 91 ' . self::API_URI);
		

		foreach($html->find('a[title^=]') as $linkElement) {
			$item = array();
			
			$item['uri'] = self::URI . "/" . $html->find('a', 0)->getAttribute('href');
			
			$html = getSimpleHTMLDOM($item['uri']) or returnServerError(' 91 ' . $item['uri']);
			
			$item['author'] = $html->find('a', 0)->getAttribute('href');
			$item['title'] = $html->find('#threadtitle > h1', 0)->plaintext;
			$time = $html->find('.authorinfo > [id^=authorposton] > span', 0)->getAttribute('title');
			$item['timestamp'] = date('c', strtotime($time));
			
			$sections = $html->find('#postlist > div:nth-child(1) td[id^=postmessage_], #postlist > div:nth-child(1) div.postattachlist, #postlist > div:nth-child(1) div.postattachlist, #wrap div.postbox > div.alert_error');
			$content;
			foreach($sections as $section) {
				$content = $content . $section->innertext;
			}
			$item['content'] = $content;
			
			$this->items[] = $item;
		}
	}
}
