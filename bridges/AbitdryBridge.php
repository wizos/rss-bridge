<?php
class AbitdryBridge extends BridgeAbstract {
	const MAINTAINER = 'Wizos';
	const NAME = 'A Bit Dry';
	const URI = 'https://abitdry.com';
	const DESCRIPTION = '一个独立开发者和他的生活方式';
	const CACHE_TIMEOUT = 86400; // 1天

	public function collectData(){
		$html = getSimpleHTMLDOM(self::URI) or returnServerError('无法请求 A Bit Dry');
		
		foreach($html->find('main[role=main] > div > a') as $linkElement) {
			$item = array();
			
			$item['author'] = self::NAME;
			$item['title'] = $linkElement->plaintext;
			$time = $linkElement->find('span', 0)->plaintext;
			$arr = date_parse_from_format('Y年m月d日', $time);
			$item['timestamp'] = mktime(0,0,0,$arr['month'],$arr['day'],$arr['year']);
			
			$item['uri'] = self::URI . $linkElement->href;
			
			$html = getSimpleHTMLDOM($item['uri']) or returnServerError('无法请求 A Bit Dry 的文章：' . $item['uri']);
			
			$article = $html->find('body > main > div > article', 0);
			// $article->find('h1:nth-child(1)', 0)->remove();
			foreach($article->find('h1:nth-child(1)') as $useless) {
				$useless->remove();
			}
			$item['content'] = $article->innertext;
			
			$this->items[] = $item;
		}
	}
}
